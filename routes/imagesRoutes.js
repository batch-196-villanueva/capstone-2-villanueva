const express = require("express");
const router = express.Router();
const store = require("../multer.js")


const imageControllers = require("../controllers/imageControllers");

const auth = require("../auth");

const { verify } = auth;
const { verifyAdmin } = auth;

router.get("/view/:imageId", imageControllers.viewImage);
router.post("/upload/:productId", store.array('photo',12),imageControllers.uploadImage);


module.exports = router;