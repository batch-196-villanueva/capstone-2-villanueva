/*
	Model for products collection
*/
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type:String,
		required: [true,"Product name required"]
	},
	description: {
		type: String,
		required: [true,"Description required"]
	},
	price: {
		type: Number,
		required: [true,"Price required"]
	},
	numberOfOrders: {
		type: Number,
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true
	},
    createdOn: {
		type: Date,
		default: new Date()
	},
    updatedOn: {
        type: Date,
        default: new Date()
    },
	images: [
		{imageId: {
			type: String,
			required: [true,"Image Id required"]
		}}
	]
	})

	module.exports = mongoose.model("Product",productSchema);