const Image = require("../models/Image")
const Product = require("../models/Product")
const fs = require('fs')

// for viewing images
// module.exports.viewImages =  (req, res) => {
//     Image.find()
//     .then(result => res.send(result))
//     .catch(error => res.send(error))
// }

//for view image
module.exports.viewImage = (req, res) => {
    Image.findById(req.params.imageId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

// for uploading images
module.exports.uploadImage = (req,res,next) => {
    const files = req.files;

    if(!files){
        const error = new Error('Please choose files');
        error.httpStatusCode = 400;
        return next(error)
    }

    // convert images into base64 encoding
    let imgArray = files.map((file) => {
        let img = fs.readFileSync(file.path)
        
        return encode_image = img.toString('base64')
    })

    imgArray.map((src, index)=>{
        let finalImg = {
            filename: files[index].originalname,
            contentType: files[index].mimetype,
            imageBase64: src
        }

        let newUpload = new Image(finalImg);
        
        newUpload.save()
        .then(result => 
            Product.findByIdAndUpdate(req.params.productId, 
                {$push: {images: {imageId : result._id}}},{new:true}
                )
            )
        .catch(err => console.log(err))
    })

    res.send(true)

}