const express = require("express");

const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;

// setting up connection to mongoDB
mongoose.connect("mongodb+srv://admin:admin123@cluster0.wqkgh.mongodb.net/capstone-2-villanueva?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB."))

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//for userRoutes
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

//for productRoutes
const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

//for orderRoutes
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);

// for imagesRoutes
const imagesRoutes = require('./routes/imagesRoutes');
app.use('/images',imagesRoutes);

app.listen(port,()=>console.log("Ecommerce API running at localhost:4000"))