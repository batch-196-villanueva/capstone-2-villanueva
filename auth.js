const jwt = require("jsonwebtoken");

const secret = "capstone2";

module.exports.createAccessToken = (userDetails) => {
	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin,
		hasFullControl: userDetails.hasFullControl
	}
	//jwt.sign() will create a JWT using our data object, with our secret.
	return jwt.sign(data,secret,{});

}

// Verify if authenticated user
module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."});
	} else {
		token = token.slice(7);
		jwt.verify(token,secret,function(err,decodedToken){
			if(err){
				return res.send({
					auth: "No such user exists! Please try again.",
					message: err.message
				})
			} else {
				req.user = decodedToken;
				next();

			}

		})
	}

}

// Verify if authenticated user is an admin
module.exports.verifyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "No permission"
        })
    }
}

// Verify that an admin has full control
module.exports.verifyFullControl = (req,res,next) =>{
	if(req.user.hasFullControl && req.user.isAdmin){
		next()
	} else {
        return res.send({
            auth: "Failed",
            message: "No permission"
        })
    }
}

